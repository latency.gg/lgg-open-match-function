package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/latency.gg/lgg-open-match-function/pkg/function"
	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"
)

const (
	queryServiceAddr = "open-match-query.open-match.svc.cluster.local:50503"
	serverPort       = 50502
)

func main() {
	Start(queryServiceAddr, serverPort)
}

func Start(queryServiceAddr string, serverPort int) {
	conn, err := grpc.Dial(queryServiceAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to Open Match, got %s", err.Error())
	}
	defer conn.Close()

	service := function.MatchFunctionService{
		QueryServiceClient: pb.NewQueryServiceClient(conn),
	}

	// Create and host a new gRPC service on the configured port.
	server := grpc.NewServer()
	pb.RegisterMatchFunctionServer(server, &service)

	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", serverPort))
	if err != nil {
		log.Fatalf("TCP net listener initialization failed for port %v, got %s", serverPort, err.Error())
	}

	log.Printf("TCP net listener initialized for port %v", serverPort)
	err = server.Serve(ln)
	if err != nil {
		log.Fatalf("gRPC serve failed, got %s", err.Error())
	}
}
