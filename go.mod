module gitlab.com/latency.gg/lgg-open-match-function

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	gitlab.com/latency.gg/lgg-open-match-spec v0.0.1
	google.golang.org/grpc v1.45.0
	open-match.dev/open-match v1.3.0
)

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0 // indirect
	golang.org/x/net v0.0.0-20211101193420-4a448f8816b3 // indirect
	golang.org/x/sys v0.0.0-20211103235746-7861aae1554b // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20211102202547-e9cf271f7f2c // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
